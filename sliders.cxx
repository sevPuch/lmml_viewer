#include "sliders.h"

#include "Viewer.h"
#include <string>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>

using namespace std;

vtkSliderCallback* vtkSliderCallback::New()
{
    return new vtkSliderCallback;
}

// Callback function, called on every slider value modification
void vtkSliderCallback::Execute(vtkObject *caller, unsigned long, void *)
{
    vtkSliderWidget *sliderWidget =
        reinterpret_cast<vtkSliderWidget *>(caller);
    this->_opacity = static_cast<vtkSliderRepresentation *>(sliderWidget->GetRepresentation())->GetValue();
    float opacityFloat = (float)_opacity;

    UpdateOpacities(opacityFloat, _tissueToUse);
}

// Sets the tissue associated with this slider
void vtkSliderCallback::SetTissueName(string tissueName)
{
    this->_tissueToUse = (string) tissueName;
}

void vtkSliderCallback::SetOpacity(double opacity)
{
    this->_opacity = (double) opacity;
}
