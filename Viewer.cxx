#include "Viewer.h"
#include "sliders.h"
#include "KeyPressInteractorStyle.h"
#include "ActorSelector.h"

#include <type_traits>
#include <typeinfo>
#include <vector>
#include <map>
#include <sstream>
#include <iostream>
#include <iterator>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>

#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkMetaImageReader.h>
#include <vtkNamedColors.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkMarchingCubes.h>
#include <vtkClipVolume.h>

#include <vtkPlaneSource.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include <vtkSphereSource.h>
#include <vtkProbeFilter.h>
#include <vtkSphere.h>
#include <vtkClipDataSet.h>
#include <vtkImplicitVolume.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#include <vtkPlane.h>
#include <vtkVersion.h>
#include <vtkRendererCollection.h>
#include <vtkIdTypeArray.h>
#include <vtkTriangleFilter.h>
#include <vtkCommand.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>

#include <array>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include "vtkInteractorStyleTrackballCamera.h"
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkLineSource.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>
#include <vtkOrientationMarkerWidget.h>

#include <vtkTransform.h>
#include <vtkAxesActor.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>

using namespace boost::property_tree;
using namespace std;

// Simplifies instantiating of vtk classes :
#define Instantiate(class, name) vtkSmartPointer<class> name = vtkSmartPointer<class>::New();

//// Rendering related
bool volumeRendering = true;  // For enabling surface-like rendering
bool switchBool = false;      // Used for switching between opacity interpolation modes
int densityBrowsingRate = 50; // Amount of density change per step (default: 50)
int densityPeakRadius = 500;  // Influences radius of density peak (default: 500)
float pushForce = 1.5f;       // Amount of displacement per step for the cutting plane
// Dictionary of tissues and matching opacities :
map<string, int> importantTissues = {{"bone", 1350}, {"muscle", 900}, {"air", 200}, {"water", 0}, {"fat", -200}};
map<string, int> purifiedTagsOpacities; // Used to catch tissue tags from patient's JSON file
vector<int> currentDensities;           // List of current densities to use

//// JSON related
string CT_FileName;        // Name of CT file to open
string jsonFileName;       // JSON file with patient's data, obtained from Malik's LMML server (lmml-on-rails)
ptree activeProfileTree;   // Used for browsing JSON files
vector<string> allTissues; // List of tissues used for an active profile
string currentTissue;
string presetName;                                        // Preset to use for opacities, obtained through automatic analysis of jsonFileName
vector<boost::property_tree::ptree::value_type> injuries; // List of all injuries present in the patient's JSON file

// Methods
void InitializePresets(string _tag);
void UpdatePresets(string _tag);
void MapOpacityValues(map<string, int> tissuesMap, float targetOpacity);
void AssignOpacities(map<pair<string, int>, float> _opacities, int _targetOpacityValue);
void TuneDensityPeaks();
void MovePlaneUp(vtkSmartPointer<vtkVolume> _volume);
void MovePlaneDown(vtkSmartPointer<vtkVolume> _volume);
void SwitchInterpolationMode();
void ChangeCurrentTissue();
vector<string> ReadJson(string _jsonFileName);
void CatchTags(vector<string> allTags);

// Global VTK components
Instantiate(vtkPiecewiseFunction, volumeScalarOpacity);
Instantiate(vtkColorTransferFunction, volumeColor);
Instantiate(vtkVolume, volume);
Instantiate(vtkRenderer, renMain);
Instantiate(vtkRenderWindow, renWinMain);
Instantiate(vtkRenderWindowInteractor, irenMain);
Instantiate(vtkPlane, cuttingPlane);
Instantiate(vtkVolumeProperty, volumeProperty);

// Variables and methods related to SliderCallback (updating sliders' values)
vector<vtkSmartPointer<vtkSliderRepresentation2D>> sliders;
vector<vtkSmartPointer<vtkSliderWidget>> sliderWidgets;
vector<vtkSmartPointer<vtkSliderCallback>> sliderCallbacks;
vector<string> sliderTissues;
vector<double> sliderOpacities;
void InitializeSliders(string _tissue, double opacityToUse, vtkSmartPointer<vtkSliderRepresentation2D> _slider,
                       vtkSmartPointer<vtkSliderWidget> _sliderWidget,
                       vtkSmartPointer<vtkSliderCallback> _callback, int yPos);

Viewer *Viewer::New()
{
  return new Viewer;
}

// Main program
int main(int argc, char *argv[])
{
  //// Initialisation :
  Viewer *viewer = Viewer::New();                                                  // Main class
  viewer->iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();                // vtk Interactor
  viewer->renWin = vtkSmartPointer<vtkRenderWindow>::New();                        // vtk renderWindow
  viewer->ren = vtkSmartPointer<vtkRenderer>::New();                               // vtk renderer
  viewer->volumeMapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New(); // vtk mapper for volume

  // Local references for usage out of main loop
  irenMain = viewer->iren;
  renWinMain = viewer->renWin;
  renMain = viewer->ren;

  // Info window at the right side (deprecated in the Qt version of the software)
  viewer->infoRen = vtkSmartPointer<vtkRenderer>::New();
  viewer->title = vtkSmartPointer<vtkTextActor>::New();
  viewer->content = vtkSmartPointer<vtkTextActor>::New();
  viewer->footer = vtkSmartPointer<vtkTextActor>::New();

  // VTK interactor styles - modified classes (determine how mouse/keyboard behave)
  KeyPressInteractorStyle *keyPress = KeyPressInteractorStyle::New();
  ActorSelector *actorSelector = ActorSelector::New();
  viewer->SetInteractor(keyPress);
  viewer->SetSelector(actorSelector);
  keyPress->SetViewer(viewer);
  actorSelector->SetViewer(viewer);

  // Surface-like rendering
  string answer;
  std::cout << "Do you want to enable surface-like rendering ? (y/n) :" << endl;
  std::cin >> answer;
  if (answer == "y" || answer == "Y" || answer == "yes" || answer == "Yes")
    volumeRendering = false;
  else
    volumeRendering = true;

  // Analyse patient's JSON file and get tags from it
  jsonFileName = "../json/patient";
  vector<string> descriptionTable;
  descriptionTable = ReadJson(jsonFileName);
  CatchTags(descriptionTable);

  // Get most used tag in patient file :
  // Here the description table needs to be sorted by number of times mentionned in the patient's file.
  // This function hasn't been yet fully implemented.
  // So getting the first element by default.
  presetName = descriptionTable[0];

  // Initialise activeProfile according to most used tag :
  InitializePresets(presetName);

  // Tune density peaks :
  TuneDensityPeaks();

  ///////////////////////////
  // RENDERING COMPONENTS
  //////////////////////////

  // Set up rendering components :
  currentTissue = allTissues[1];
  cout << "Current tissue is : " << currentTissue << endl;
  CT_FileName = "../Data/FullHead.mhd";
  Instantiate(vtkNamedColors, colors); // Colors definition component
  std::array<unsigned char, 4> bkg{{230, 230, 230, 230}};
  colors->SetColor("BkgColor", bkg.data());
  renWinMain->AddRenderer(renMain); // Add renderer to the render window

  // Window size (one window, two renderers)
  renWinMain->SetSize(1200, 600);
  viewer->winSize = renWinMain->GetActualSize();

  // Define viewport ranges (xmin, ymin, xmax, ymax)
  double _leftViewport[4] = {0.0, 0.0, 0.6, 1.0};
  double _rightViewport[4] = {0.6, 0.0, 1.0, 1.0};
  viewer->leftViewport = _leftViewport;
  viewer->rightViewport = _rightViewport;

  // Left renderer
  renMain->SetBackground(colors->GetColor3d("BkgColor").GetData());
  renMain->SetViewport(viewer->leftViewport);
  viewer->SetUpInfoRenderer();

  // Reader of the CT data
  Instantiate(vtkMetaImageReader, reader);
  const char *CT_cstr = CT_FileName.c_str(); // Parse the data path
  cout << CT_cstr << endl;
  reader->SetFileName(CT_cstr);
  reader->Update();

  // The volume will be displayed by ray-cast alpha compositing
  viewer->volumeMapper->SetInputConnection(reader->GetOutputPort()); // Set CT data to the volumeMapper
  Instantiate(vtkPiecewiseFunction, volumeGradientOpacity);          // More info in VTK doc, enhances opacity visibility
  volumeGradientOpacity->AddPoint(0, 0.0);
  volumeGradientOpacity->AddPoint(50, 0.2);
  volumeGradientOpacity->AddPoint(100, 1.0);

  // Assign volume properties
  volumeProperty->SetColor(volumeColor);
  volumeProperty->SetScalarOpacity(volumeScalarOpacity);
  volumeProperty->SetGradientOpacity(volumeGradientOpacity);
  SwitchInterpolationMode();
  volumeProperty->ShadeOn();
  volumeProperty->SetAmbient(0.4);
  volumeProperty->SetDiffuse(0.6);
  volumeProperty->SetSpecular(0.5);

  // The vtkVolume is a vtkProp3D (like a vtkActor) and controls the position
  // and orientation of the volume in world coordinates.
  // Assign mapper and properties to the vtkVolume :
  volume->SetMapper(viewer->volumeMapper);
  volume->SetProperty(volumeProperty);

  //Plane clipping
  cuttingPlane->SetOrigin(0, 0, 100);
  cuttingPlane->SetNormal(0, 1, 0);
  viewer->volumeMapper->AddClippingPlane(cuttingPlane);

  // Finally, add the volume to the renderer
  renMain->AddViewProp(volume);
  volume->SetPosition(0, 0, 0);
  viewer->volumeCenter = volume->GetCenter();

  // Set up an initial view of the volume.
  // The focal point will be the center of the volume
  vtkSmartPointer<vtkCamera> camera = renMain->GetActiveCamera();
  viewer->camera = camera;
  camera->SetViewUp(0, 0, -1);
  camera->SetPosition(viewer->volumeCenter[0], viewer->volumeCenter[1] - 400, viewer->volumeCenter[2]);
  camera->SetFocalPoint(viewer->volumeCenter[0], viewer->volumeCenter[1], viewer->volumeCenter[2]);
  //camera->Azimuth(30.0);
  camera->Elevation(30.0);

  // Set up interactors
  viewer->iren->SetRenderWindow(renWinMain);
  viewer->iren->SetInteractorStyle(viewer->interactor); // Assign current interactor style
  viewer->interactor->SetCurrentRenderer(renMain);
  viewer->interactor->SetDefaultRenderer(renMain);
  viewer->selector->SetCurrentRenderer(renMain);
  viewer->selector->SetDefaultRenderer(renMain);

  ///////////////////////////
  // USER INTERFACE
  //////////////////////////

  // Sliders initialization (for modifying opacities)
  // Create a slider for each value present in the active profile :
  int sliderCount = 0;
  BOOST_FOREACH (boost::property_tree::ptree::value_type &values, activeProfileTree.get_child("values."))
  {
    string tissueToPass;
    double opacityToPass;

    tissueToPass = values.first;

    stringstream o(values.second.get<string>("opacity"));
    o >> opacityToPass;

    // Slider components
    vtkSliderRepresentation2D *_slider = vtkSliderRepresentation2D::New();
    vtkSliderWidget *sliderWidget = vtkSliderWidget::New();
    vtkSliderCallback *sliderCallback = vtkSliderCallback::New();

    // Add them to vectors
    sliders.push_back(_slider);
    sliderWidgets.push_back(sliderWidget);
    sliderCallbacks.push_back(sliderCallback);

    // Slider positionning
    sliderCount++;
    cout << sliderCount << "\n";
    int _yPos = 20 + (sliderCount * 60);

    // Slider definition
    InitializeSliders(tissueToPass, opacityToPass, _slider, sliderWidget, sliderCallback, _yPos);
  }

  // Axis set up
  Instantiate(vtkAxesActor, axes);
  axes->SetTotalLength(50, 50, 50);                // Overridden by the widget definition
  Instantiate(vtkOrientationMarkerWidget, widget); // Use as a widget
  widget->SetOutlineColor(0.9300, 0.5700, 0.1300);
  widget->SetOrientationMarker(axes);
  widget->SetInteractor(viewer->iren);
  widget->SetViewport(0.8, 0.0, 1, 0.2);
  widget->SetEnabled(1);
  widget->InteractiveOn();

  // Begin mouse interaction
  viewer->ren->ResetCamera();
  viewer->renWin->Render();
  viewer->iren->Start();

  return EXIT_SUCCESS;
}

// Used at start
// Reads density/opacities/color for each tissue for the profile (_tag)
// Stores the actual preset in activeProfile.json for later modification in interaction.
void InitializePresets(string _tag)
{
  ptree pt;
  read_json("../json/presets.json", pt);
  read_json("../json/activeProfile.json", activeProfileTree);

  // Browse through each preset's profile type
  BOOST_FOREACH (boost::property_tree::ptree::value_type &profile, pt.get_child("profiles"))
  {
    string type = profile.second.get<string>("type");

    // This is the type corresponding to _tag
    if (type == _tag)
    {
      string _tissue;
      int _density;
      double _opacity;
      vector<float> _rgb;

      // Assign this type to the active profile
      activeProfileTree.put("type", type);

      // Assign every density/opacity/colors value for each tissue from preset to active profile
      BOOST_FOREACH (boost::property_tree::ptree::value_type &values, profile.second.get_child("values."))
      {
        _tissue = values.first;
        allTissues.push_back(_tissue);

        stringstream d(values.second.get<string>("density"));
        stringstream o(values.second.get<string>("opacity"));
        vector<string> c;
        BOOST_FOREACH (boost::property_tree::ptree::value_type &color, profile.second.get_child("values." + _tissue + ".color."))
        {
          c.push_back(color.second.data());
        }

        d >> _density;
        o >> _opacity;

        ptree colorNode;
        for (vector<string>::iterator itColor = c.begin(); itColor != c.end(); itColor++)
        {
          stringstream c(*itColor);
          float tempC;
          c >> tempC;
          _rgb.push_back(tempC);

          ptree colorValue;
          colorValue.put("", *itColor);
          colorNode.push_back(make_pair("", colorValue));
        }

        currentDensities.push_back(_density);
        volumeScalarOpacity->AddPoint(_density, _opacity);
        volumeColor->AddRGBPoint(_density, _rgb[0], _rgb[1], _rgb[2]);

        cout << "Successfuly assigned density of \"" << _tissue << "\" as : \"" << _density << "\" with opacity value : \""
             << _opacity << "\" to the volume." << '\n';

        // Also keep that data inside activeProfile for further interaction on opacity.
        activeProfileTree.put("values." + _tissue + ".density", _density);
        activeProfileTree.put("values." + _tissue + ".opacity", _opacity);

        int i = 0;
        BOOST_FOREACH (boost::property_tree::ptree::value_type &color, activeProfileTree.get_child("values." + _tissue + ".color."))
        {
          color.second.put("", c[i]);
          i++;
        }
        c.clear();
        _rgb.clear();
      }
    }
  }
  write_json("activeProfile.json", activeProfileTree); // Saves changes to activeProfile
}

// Called on the slider callback in vtkSliderCallback
void UpdateOpacities(float _opacityToUse, string _tissue)
{
  int _density;
  float _opacity;

  ptree tissue = activeProfileTree.get_child("values." + _tissue);

  stringstream d(tissue.get<string>("density"));
  d >> _density;

  activeProfileTree.put("values." + _tissue + ".opacity", _opacityToUse);
  write_json("activeProfile.json", activeProfileTree);

  stringstream o(tissue.get<string>("opacity"));
  o >> _opacity;

  volumeScalarOpacity->AddPoint(_density, _opacityToUse);

  volume->Update();
  irenMain->Render();
}

// Reads activeProfile and gets density/opacity values
// Called by slider value change
void UpdateDensities(int sign)
{
  int _density;
  float _opacity;

  ptree tissue = activeProfileTree.get_child("values." + currentTissue);

  stringstream d(tissue.get<string>("density"));
  stringstream o(tissue.get<string>("opacity"));

  d >> _density;
  o >> _opacity;

  _density += (sign * densityBrowsingRate);

  int counter = 0;
  for (vector<int>::iterator it = currentDensities.begin(); it != currentDensities.end(); it++)
  {
    if (*it == _density)
      counter++;
    cout << "Counter is : " << counter << " and  *it  : " << *it << " and _density : " << _density << "\n";
  }
  if (counter > 0)
  {
    _density += 4;
    cout << "counter >1 , added 4 to _density."
         << "\n";
  }
  cout << "For : " << currentTissue << ", density is : " << _density << " and opacity is : " << _opacity << "\n";

  // This methods tends to fail when density values are overlapping due to their modification
  // A workaround hasn't been yet developped

  // Get currentTissue density value, update it
  activeProfileTree.put("values." + currentTissue + ".density", _density);
  write_json("activeProfile.json", activeProfileTree);

  // Call UpdatePresets to update volumeScalarOpacity
  UpdatePresets(currentTissue);

  // Update rendering
  volume->Update();
  irenMain->Render();
}

// Called by UpdateDensities
// Sets up density/opacity values for a given tissue so it is updated in the renderer
void UpdatePresets(string _tag)
{
  string _tissue;
  int _density;
  float _opacity;
  vector<float> _rgb;

  currentDensities.clear();
  volumeScalarOpacity->RemoveAllPoints();
  volumeColor->RemoveAllPoints();

  BOOST_FOREACH (boost::property_tree::ptree::value_type &values, activeProfileTree.get_child("values."))
  {

    _tissue = values.first;

    stringstream d(values.second.get<string>("density"));
    stringstream o(values.second.get<string>("opacity"));

    vector<string> c;

    BOOST_FOREACH (boost::property_tree::ptree::value_type &color, activeProfileTree.get_child("values." + _tissue + ".color."))
    {
      c.push_back(color.second.data());
    }

    d >> _density;
    o >> _opacity;

    for (vector<string>::iterator itColor = c.begin(); itColor != c.end(); itColor++)
    {
      stringstream _c(*itColor);
      float tempC;
      _c >> tempC;
      _rgb.push_back(tempC);
    }

    currentDensities.push_back(_density);
    volumeScalarOpacity->AddPoint(_density, _opacity);
    volumeColor->AddRGBPoint(_density, _rgb[0], _rgb[1], _rgb[2]);

    cout << "Successfuly assigned density of \"" << _tissue << "\" as : \"" << _density << "\" with opacity value : \""
         << _opacity << "\" to the volume." << '\n';

    int i = 0;
    BOOST_FOREACH (boost::property_tree::ptree::value_type &color, activeProfileTree.get_child("values." + _tissue + ".color."))
    {
      color.second.put("", c[i]);
      i++;
    }
    c.clear();
    _rgb.clear();
  }

  TuneDensityPeaks();
}

// Assigns all the density peaks' values with a radius.
void TuneDensityPeaks()
{
  // Assigns all the density peaks' values with a radius.
  // Values out of radius are set to zero
  // The peak radius is controlled by the densityPeakRadius value.
  // If distance between two peaks is lower than two densityPeakRadius values, use value between those two densities.

  int differentialPeak;

  // if user wants surface-like rendering, density peaks must be very thin :
  if (volumeRendering == false) // i.e. (surface-like == true)
  {
    densityPeakRadius = 100;
  }

  // Sorts densities by value :
  sort(currentDensities.begin(), currentDensities.end());

  // Browse through densities and add those in the volumeScalarOpacity, with peak radius
  for (vector<int>::iterator it = currentDensities.begin(); it != currentDensities.end(); it++)
  {
    cout << "\nCurrent density is : " << *it
         << "\n";

    // If this is the first density in the vector
    if (it == currentDensities.begin())
    {
      volumeScalarOpacity->AddPoint(*it - densityPeakRadius, 0);
    }
    // If it's neither the first, neither the last density
    else if (*it != currentDensities.end()[-1])
    {
      // Difference between last peak and this one :
      auto diff = *next(it, 1) - *it;

      // If difference is < 2, assign a value in between :
      if (diff <= 2 * densityPeakRadius)
      {
        differentialPeak = diff / 2;
        volumeScalarOpacity->AddPoint(*it + differentialPeak, 0);
      }
      // If the difference > 2 peak radiuses, simply add new peak :
      else
      {
        volumeScalarOpacity->AddPoint(*it + densityPeakRadius, 0);
      }
    }
    // If this is the last density
    else if (it == currentDensities.end())
    {
      volumeScalarOpacity->AddPoint(*it + densityPeakRadius, 0);
    }
  }
}

// Called on KeyPressInteractorStyle ("i")
// Switches between linear or nearest interpolation mode for volume rendering
// Changes esthetics and performance
void SwitchInterpolationMode()
{
  switchBool = !switchBool;
  if (switchBool)
  {
    volumeProperty->SetInterpolationTypeToLinear();
    cout << "Switched to linear interpolation." << endl;
  }
  else
  {
    volumeProperty->SetInterpolationTypeToNearest();
    cout << "Switched to nearest interpolation." << endl;
  }
  volume->Update();
  irenMain->Render();
};

// Called on KeyPressInteractorStyle ("t")
// Changes current tissue to make it modifiable (densities are still modified by "[" and "]", not sliders)
void ChangeCurrentTissue()
{
  ptrdiff_t _currentTissuePos = distance(allTissues.begin(), find(allTissues.begin(), allTissues.end(), currentTissue));
  cout << "Position of currentTissue in allTissues is : " << _currentTissuePos << "\n";

  if (_currentTissuePos < allTissues.size() - 1)
  {
    currentTissue = allTissues[_currentTissuePos + 1];
  }
  else
  {
    currentTissue = allTissues[0];
  }
  cout << "Current tissue is : " + currentTissue + "\n";
}

// Called on KeyPressInteractorStyle ("up arrow")
void MovePlaneDown(vtkSmartPointer<vtkVolume> _volume)
{
  // Enhancement to develop for this feature :
  //  Check if plane intersects with the volume. If not, get last moving direction and block it
  //  so the plane doesn't goes far from the anatomical structure and get lost.

  cuttingPlane->Push(pushForce);
  _volume->Update();
  irenMain->Render();
}
// Called on KeyPressInteractorStyle ("down arrow")
void MovePlaneUp(vtkSmartPointer<vtkVolume> _volume)
{
  cuttingPlane->Push(-pushForce);
  _volume->Update();
  irenMain->Render();
}

// Reads a JSON file, and returns all the injuries tags in a vector<string>
vector<string> ReadJson(string _jsonFileName)
{
  ptree pt;
  vector<string> description;
  injuries.clear();
  read_json(_jsonFileName + ".json", pt);

  // Get all the examinations objects in the JSON file.
  BOOST_FOREACH (boost::property_tree::ptree::value_type &examination, pt.get_child("lmml.autopsy.examinations"))
  {
    // Get all the injuries objects in every examinations object.
    // Add it's content in vector<string>.
    BOOST_FOREACH (boost::property_tree::ptree::value_type &show, examination.second.get_child("injuries."))
    {
      injuries.push_back(show);

      string _description = show.second.get<string>("description");

      std::istringstream _stream(_description);
      vector<string> _splitText((std::istream_iterator<string>(_stream)), std::istream_iterator<string>());

      description.insert(description.end(), _splitText.begin(), _splitText.end());
    }
  }
  std::cout << "\nInjuries present in this file are :\n";
  for (vector<string>::iterator it = description.begin(); it < description.end(); it++)
  {
    std::cout << ' ' << *it;
    std::cout << '\n';
  }
  return description;
}

// Search through all tags from patient's file,
// keep every tag present in the important tissues map, add it to tag vector if not already present.
void CatchTags(vector<string> _allTags)
{
  purifiedTagsOpacities.clear();
  // Do the following for every tag present in the parameter's vector<string>.
  for (vector<string>::iterator itString = _allTags.begin(); itString < _allTags.end(); itString++)
  {
    // Iterate through every pair of the importantTissue map.
    map<string, int>::iterator iter;
    for (iter = importantTissues.begin(); iter != importantTissues.end(); iter++)
    {
      // Checks if current tag is present in importantTissues
      if (*itString == iter->first)
      {
        purifiedTagsOpacities.insert(*iter);
        cout << "\nAdded \" " << iter->first << " \" with opacity value : \" " << iter->second
             << " \" to  the range of opacity values." << endl;
        /*
        // Following is the part in charge of counting how many time each tag is used
        // The most used tag is to be used as the first preset for density/opacity values. 
        // This implementation isn't working and has to be rewritten :

        if (purifiedTagsOpacities.empty() == false)
        {
          // Finds if current density value coming from tag is already present in the purified list.
          map<string, int>::iterator itFindInOpac;
          itFindInOpac = purifiedTagsOpacities.begin();
          int opacToTest = iter->second;
          bool valueAlreadyPresent = false;

          while (itFindInOpac != purifiedTagsOpacities.end())
          {
            if (opacToTest == itFindInOpac->second)
            {
              valueAlreadyPresent = true;
              
            }
            itFindInOpac++;
          }
          if (valueAlreadyPresent == false)
          {
            cout << "The already present bool is : " << valueAlreadyPresent << "\n";
            purifiedTagsOpacities.insert(*iter);
            cout << "Added \" " << iter->first << " \" with opacity value : \" " << iter->second
                 << " \" to  the range of opacity values." << endl;
          }
        }
        // if list is empty :
        else 
        {
          purifiedTagsOpacities.insert(*iter);
          cout << "Added \" " << iter->first << " \" with opacity value : \" " << iter->second
               << " \" to  the range of opacity values." << endl;
        }*/
      }
    }
  }
}

void InitializeSliders(string tissueToUse, double opacityToUse, vtkSmartPointer<vtkSliderRepresentation2D> slider,
                       vtkSmartPointer<vtkSliderWidget> _sliderWidget,
                       vtkSmartPointer<vtkSliderCallback> _callback, int yPos)
{
  slider->SetMinimumValue(0.0);
  slider->SetMaximumValue(1.0);
  slider->SetValue(opacityToUse);

  string sliderString = tissueToUse;
  sliderString[0] = toupper(sliderString[0]);
  sliderString.append(" opacity");
  const char *sliderText = sliderString.c_str();
  slider->SetTitleText(sliderText);

  slider->GetSliderProperty()->SetColor(0.05, 0.05, 0.05);
  slider->GetTitleProperty()->SetColor(0, 0, 0);
  slider->GetLabelProperty()->SetColor(0, 0, 0);
  slider->GetSelectedProperty()->SetColor(0.1, 0.1, 0.1);
  slider->GetTubeProperty()->SetColor(0.3, 0.3, 0.5);
  slider->GetCapProperty()->SetColor(0.3, 0.3, 0.5);

  slider->GetPoint1Coordinate()->SetCoordinateSystemToDisplay();
  slider->GetPoint1Coordinate()->SetValue(20, yPos);
  slider->GetPoint2Coordinate()->SetCoordinateSystemToDisplay();
  slider->GetPoint2Coordinate()->SetValue(160, yPos);

  _callback->SetOpacity(opacityToUse);
  _callback->SetTissueName(tissueToUse);

  _sliderWidget->SetInteractor(irenMain);
  _sliderWidget->SetRepresentation(slider);
  _sliderWidget->SetAnimationModeToAnimate();
  _sliderWidget->EnabledOn();
  _sliderWidget->AddObserver(vtkCommand::InteractionEvent, _callback);
}

void Viewer::SetInteractor(KeyPressInteractorStyle *_interactor)
{
  this->interactor = _interactor;
}

void Viewer::SetSelector(ActorSelector *_selector)
{
  this->selector = _selector;
}

// Deprecated
void Viewer::TweakInteractorStyle()
{
  iren->SetInteractorStyle(this->selector);

  this->selector->SetCurrentRenderer(ren);
  this->selector->SetDefaultRenderer(ren);
}

// Deprecated in Qt version
void Viewer::SetUpInfoRenderer()
{
  infoRen = vtkSmartPointer<vtkRenderer>::New();
  renWinMain->AddRenderer(infoRen);
  infoRen->SetViewport(rightViewport);
  infoRen->SetBackground(0.8, 0.8, 0.8);
  infoRen->ResetCamera();
  // Initialise texts
  title = vtkSmartPointer<vtkTextActor>::New();
  content = vtkSmartPointer<vtkTextActor>::New();
  footer = vtkSmartPointer<vtkTextActor>::New();
  // Text properties
  title->SetInput("");
  title->SetPosition(15, winSize[1] - titleSize - 5);
  title->GetTextProperty()->SetFontSize(20);
  title->GetTextProperty()->SetColor(0.05, 0.05, 0.05);
  content->SetPosition(15, winSize[1] - titleSize * 2 - 10);
  content->GetTextProperty()->SetFontSize(12);
  content->GetTextProperty()->SetColor(0.05, 0.05, 0.05);
  footer->SetInput("Create a label with CTRL + RMB.\nYou can then select it with CTRL + LMB.");
  footer->SetPosition(15, footerSize * 2);
  footer->GetTextProperty()->SetFontSize(14);
  footer->GetTextProperty()->SetColor(0.05, 0.05, 0.05);

  infoRen->AddActor2D(title);
  infoRen->AddActor2D(content);
  infoRen->AddActor2D(footer);
  texts.push_back(title);
  texts.push_back(content);
  texts.push_back(footer);
}

// Deprecated in the Qt version
// Updates text in info bar
void Viewer::SetText(int element, string _text)
{
  const char *txt = _text.c_str();
  switch (element)
  {
  case 0:
    this->title->SetInput(txt);
    break;
  case 1:
    this->content->SetInput(txt);
    break;
  case 2:
    this->footer->SetInput(txt);
    break;
  }
}