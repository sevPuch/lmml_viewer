#ifndef SLIDERS_H
#define SLIDERS_H

#include "Viewer.h"
#include <string>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>

class vtkSliderCallback : public vtkCommand
{
public:
  static vtkSliderCallback *New();
  void Execute(vtkObject *caller, unsigned long, void *);
  void SetTissueName(string tissueName);
  void SetOpacity(double opacity);
  double _opacity;
  string _tissueToUse;
};

#endif