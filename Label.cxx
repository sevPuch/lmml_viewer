#include "Label.h"
#include <math.h>
#include <string>
#include <vector>
#include <iterator>

#include <vtkInteractorObserver.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

using namespace std;

Label::Label(void)
{
    cout << "A Label has been constructed.";
}

Label *Label::New()
{
    return new Label;
}

// Creates label with all of it's components (spheres and line)
void Label::SetUpLabel(double *_worldPosition)
{
    sphere = vtkSmartPointer<vtkSphereSource>::New();
    sphere->SetCenter(_worldPosition[0], _worldPosition[1], _worldPosition[2]);
    sphere->SetRadius(3);

    sphereMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    sphere->SetPhiResolution(32);
    sphere->SetThetaResolution(32);
    sphereMapper->SetInputConnection(sphere->GetOutputPort());

    sphereActor = vtkSmartPointer<vtkActor>::New();
    sphereActor->SetMapper(sphereMapper);
    sphereActor->GetProperty()->SetColor(1, 0, 0);
    sphereActor->GetProperty()->SetOpacity(1);
    sphereActor->GetProperty()->SetDiffuse(0.5);
    sphereActor->GetProperty()->SetSpecular(0.5);
    sphereActor->DragableOn();

    // Set up point 2
    sphere2 = vtkSmartPointer<vtkSphereSource>::New();
    sphere2->SetCenter(0, 0, 0);
    sphere2->SetRadius(6);

    sphereMapper2 = vtkSmartPointer<vtkPolyDataMapper>::New();
    sphere2->SetPhiResolution(4);
    sphere2->SetThetaResolution(4);
    sphereMapper2->SetInputConnection(sphere2->GetOutputPort());

    sphereActor2 = vtkSmartPointer<vtkActor>::New();
    sphereActor2->SetMapper(sphereMapper2);
    sphereActor2->GetProperty()->SetColor(1, 0, 0);
    sphereActor2->GetProperty()->SetOpacity(1);
    sphereActor2->GetProperty()->SetDiffuse(0.5);
    sphereActor2->GetProperty()->SetSpecular(0.5);

    // Calculate if label on left or right.
    double *p1Center = this->sphereActor->GetCenter();
    double *c = viewer->volumeCenter;

    double point1Screen[3];
    double cScreen[3];

    vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p1Center[0], p1Center[1], p1Center[2], point1Screen);
    vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);

    sphereActor2->SetPosition(this->labelOffset, _worldPosition[1], _worldPosition[2]);

    // Create line
    line = vtkSmartPointer<vtkLineSource>::New();
    line->SetPoint1(_worldPosition);

    lineMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    lineMapper->SetInputConnection(line->GetOutputPort());
    lineActor = vtkSmartPointer<vtkActor>::New();
    lineActor->SetMapper(lineMapper);
    lineActor->GetProperty()->SetLineWidth(2);
    lineActor->GetProperty()->SetColor(1, 0, 0);

    // Create some text
    nameTextSource = vtkSmartPointer<vtkVectorText>::New();
    textMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    textMapper->SetInputConnection(nameTextSource->GetOutputPort());

    // Create a subclass of vtkActor: a vtkFollower that remains facing the camera
    nameFollower = vtkSmartPointer<vtkFollower>::New();
    nameFollower->SetMapper(textMapper);
    nameFollower->GetProperty()->SetColor(0, 0, 0);
    nameFollower->SetScale(6, 6, 6);

    // Apply to line and follower positions :
    line->SetPoint2(sphereActor2->GetPosition());
    nameFollower->SetPosition(7, _worldPosition[1], _worldPosition[2]);

    this->UpdatePoint2();
}

void Label::SetViewer(Viewer *_viewer)
{
    this->viewer = _viewer;
}

// Point 2 is the point near the injury name. 
// This method makes it always visible to the user when the camera is moved.
void Label::UpdatePoint2()
{
    this->angle = this->CalculateDot();

    // If angle acceptable, don't change position.
    if (this->angle > 1.5 && this->angle < 1.6)
    {
        return;
    }
    cout << "\n=========================================\n"
         << "HERE IS LABEL  " << this->id << "  's UpdatePoint2()\n";

    // Set up needed vars for screen projection :
    double *p1Center = this->sphereActor->GetCenter();
    double *p2Center = this->sphereActor2->GetCenter();
    double *c = viewer->volumeCenter;

    vtkSmartPointer<vtkTransform>
        transform = vtkSmartPointer<vtkTransform>::New();
    transform->PostMultiply();

    // Calculate screen projection of p2 and c
    double point1Screen[3];
    double point2Screen[3];
    double cScreen[3];
    vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p1Center[0], p1Center[1], p1Center[2], point1Screen);
    vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p2Center[0], p2Center[1], p2Center[2], point2Screen);
    vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);

    // If point1Screen is greater than cScreen
    if (point1Screen[0] >= cScreen[0])
    {
        p2Center = this->MakePerpendicular(transform, p2Center, 1);
        vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p2Center[0], p2Center[1], p2Center[2], point2Screen);
        vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);

        // If wrong side of the model, apply 180 degree rotation.
        if (point2Screen[0] < cScreen[0])
        {
            cout << " Applying rotation of 180 degrees around Z...\n/n";
            transform->Translate(-c[0], -c[1], -c[2]);
            transform->RotateWXYZ(180, 0, 0, 1);
            transform->Translate(c);
            this->sphereActor2->SetUserTransform(transform);

            p2Center = this->sphereActor2->GetCenter();
            line->SetPoint2(p2Center);
            line->Update();

            //this->angle = this->CalculateDot();
            //vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p2Center[0], p2Center[1], p2Center[2], point2Screen);
            //vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);
        }
    }
    else
    {
        p2Center = this->MakePerpendicular(transform, p2Center, 1);
        vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p2Center[0], p2Center[1], p2Center[2], point2Screen);
        vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);

        // If wrong side of the model, apply 180 degree rotation.
        if (point2Screen[0] > cScreen[0])
        {
            cout << " Applying rotation of 180 degrees around Z...\n/n";
            transform->Translate(-c[0], -c[1], -c[2]);
            transform->RotateWXYZ(180, 0, 0, 1);
            transform->Translate(c);
            this->sphereActor2->SetUserTransform(transform);

            p2Center = this->sphereActor2->GetCenter();
            line->SetPoint2(p2Center);
            line->Update();

            //this->angle = this->CalculateDot();

            //vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, p2Center[0], p2Center[1], p2Center[2], point2Screen);
            //vtkInteractorObserver::ComputeWorldToDisplay(this->viewer->ren, c[0], c[1], c[2], cScreen);
        }
    }

    p2Center = this->sphereActor2->GetCenter();
    line->SetPoint2(p2Center);
    line->Update();
    nameFollower->SetPosition(p2Center[0], p2Center[1], p2Center[2] + 10);
}

// Calculates angle between Point 2 and the camera, relatively to the volume's center. 
double Label::CalculateDot()
{
    double *p2Center = this->sphereActor2->GetCenter();
    double *c = viewer->volumeCenter;
    double *cameraPos = this->viewer->camera->GetPosition();

    double camOrig[3] = {
        cameraPos[0] - c[0],
        cameraPos[1] - c[1],
        cameraPos[2] - c[2]};

    double p2Orig[3] = {
        p2Center[0] - c[0],
        p2Center[1] - c[1],
        p2Center[2] - c[2]};

    double angle = vtkMath::AngleBetweenVectors(camOrig, p2Orig);

    return angle;
}

// Rotates the Point 2 around the volume's center.
double *Label::MakePerpendicular(vtkTransform *transform, double *p2Center, int direction)
{
    if (direction < 0)
    {
        direction = -1;
    }
    else
    {
        direction = 1;
    }
    this->angle = this->CalculateDot();
    double *c = this->viewer->volumeCenter;

    while (this->angle < 1.5 || this->angle > 1.6)
    {
        transform->Translate(-c[0], -c[1], -c[2]);
        transform->RotateWXYZ(direction, 0, 0, 1);
        transform->Translate(c);
        this->sphereActor2->SetUserTransform(transform);

        p2Center = this->sphereActor2->GetCenter();
        line->SetPoint2(p2Center);
        line->Update();

        this->angle = this->CalculateDot();
    }
    return p2Center;
}