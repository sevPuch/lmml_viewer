#ifndef ACTORSELECTOR_H
#define ACTORSELECTOR_H

#include <vector>
#include <array>
#include "Viewer.h"
#include "KeyPressInteractorStyle.h"
#include "Label.h"
#include "vtkInteractorStyleTrackballActor1.h"
#include "vtkLineSource.h"

using namespace std;

class Viewer;
class Label;
class ActorSelector : public vtkInteractorStyleTrackballActor1
{
public:
  Viewer *viewer;
  // vtkActor* _labelActor;
  Label *currentLabel;
  vector<Label *> labels;

  vtkSmartPointer<vtkCellPicker> picker;

  bool labelInhibited = false;
  double offset;
  vector<array<double, 3>> point2List;
  vector<array<double, 3>> textPosList;

  // Screen cell picker coordinates.
  int x;
  int y;

  static ActorSelector *New();
  vtkTypeMacro(ActorSelector, vtkInteractorStyleTrackballActor1);

  void OnLeftButtonDown();
  void OnRightButtonDown();
  void OnRightButtonUp();
  void OnKeyUp();

  void SetViewer(Viewer *_viewer);
  void SetPos();
  double *PickOnVolume(vtkCellPicker *_picker);

  ActorSelector() {}
};

#endif