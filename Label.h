#ifndef LABEL_H
#define LABEL_H

#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkVectorText.h>
#include <vtkLineSource.h>
#include <vtkFollower.h>

#include "Viewer.h"
#include "ActorSelector.h"

class Viewer;
class ActorSelector;
class Label
{
public:
  Label();
  static Label *New();
  Viewer *viewer;
  ActorSelector *selector;

  vtkSmartPointer<vtkSphereSource> sphere;
  vtkSmartPointer<vtkPolyDataMapper> sphereMapper;
  vtkSmartPointer<vtkActor> sphereActor;

  vtkSmartPointer<vtkSphereSource> sphere2;
  vtkSmartPointer<vtkPolyDataMapper> sphereMapper2;
  vtkSmartPointer<vtkActor> sphereActor2;

  vtkSmartPointer<vtkLineSource> line;
  vtkSmartPointer<vtkPolyDataMapper> lineMapper;
  vtkSmartPointer<vtkActor> lineActor;

  vtkSmartPointer<vtkVectorText> nameTextSource;
  vtkSmartPointer<vtkPolyDataMapper> textMapper;
  string labelContent;
  vtkSmartPointer<vtkFollower> nameFollower;

  int id;
  string title;
  string content;
  string footer;

  float labelOffset = 5;
  int x;
  int y;
  double angle;

  void SetUpLabel(double *_worldPosition);
  void SetViewer(Viewer *_viewer);
  void UpdatePoint2();
  double CalculateDot();
  double *MakePerpendicular(vtkTransform *transform, double *p2Center, int direction);

  //Label() {}
};

#endif