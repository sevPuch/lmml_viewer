#include "Viewer.h"
#include "ActorSelector.h"
#include "KeyPressInteractorStyle.h"
#include "Label.h"
#include "vtkInteractorStyleTrackballActor1.h"
#include "vtkInteractorObserver.h"

#include <vector>
#include <iterator>

using namespace std;

ActorSelector *ActorSelector::New()
{
    return new ActorSelector;
}

void ActorSelector::SetViewer(Viewer *_viewer)
{
    this->viewer = _viewer;
    cout << this->viewer->ren->GetClassName() << "\n";
    picker = vtkSmartPointer<vtkCellPicker>::New();
}

// Selects a label actor
// Updates colors for active/inactive labels
void ActorSelector::OnLeftButtonDown()
{
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    this->FindPokedRenderer(x, y);
    this->FindPickedActor(x, y);
    if (this->CurrentRenderer == nullptr || this->InteractionProp == nullptr)
    {
        return;
    }

    this->GrabFocus(this->EventCallbackCommand);

    // iterate through labels and change colors
    for (Label *label : this->viewer->labels)
    {
        if (this->InteractionProp == label->sphereActor)
        {
            double *c = label->sphereActor->GetCenter();
            cout << c[0] << "   " << c[1] << "   " << c[2];
            label->sphereActor->GetProperty()->SetColor(0, 0, 1);

            currentLabel = label;
        }
        else
        {
            label->sphereActor->GetProperty()->SetColor(1, 0, 0);
        }
    }
    viewer->iren->Render();

    // Display label's infos on infoRen
    // To be replaced with Qt callbacks
    if (!currentLabel->title.empty())
    {
        viewer->SetText(0, currentLabel->title);
        viewer->SetText(1, currentLabel->content);
        viewer->SetText(2, currentLabel->footer);
    }
    else
    {
        viewer->SetText(2, "This label is still empty. \nPlease choose an injury to fill in, or write \nyour own text by clicking \"New text\".");
    }
    viewer->infoRen->Render();

    // Forward event
    vtkInteractorStyleTrackballActor1::OnLeftButtonDown();
}

// Creates a new label at click position
void ActorSelector::OnRightButtonDown()
{
    if (!labelInhibited)
    {
        SetPos();

        string className = this->InteractionProp->GetClassName();

        //cout << this->InteractionProp->GetClassName() << "\n";
        //pos = this->GetInteractor()->GetEventPosition();

        if (this->CurrentRenderer == nullptr || this->InteractionProp == nullptr)
        {
            cout << "  This point isn't located on a labelable surface.";
            return;
        }
        else if (className == "vtkVolume")
        {
            //Label *label = Label::New();
            Label *label;
            label = new Label;
            this->viewer->labels.push_back(label);

            label->SetViewer(this->viewer);

            double *worldPosition = PickOnVolume(picker);

            // Initiates label's components according to picked worldPosition.
            label->SetUpLabel(worldPosition);

            // Assign label text
            label->labelContent = "Injury " + to_string(static_cast<int>(this->viewer->labels.size()));
            auto _labelChar = label->labelContent.c_str();
            label->nameTextSource->SetText(_labelChar);
            label->nameFollower->SetCamera(this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera());

            // Label's id :
            label->id = static_cast<int>(this->viewer->labels.size());

            // Add label actors to the renderer.
            this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(label->sphereActor);
            this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(label->sphereActor2);
            this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(label->nameFollower);
            this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor(label->lineActor);

            // Calculate on which side is the point 2 of the label

            // ...

            labelInhibited = true;
        }
        // Makes sure labels don't stack on eachother, and that only the volume can be labelled
        else if (className == "vtkOpenGLActor" ||
                 className == "vtkFollower")
        {
            cout << "  This point isn't labelable, labels can't be stacked."
                 << "\n  You can eventually add information to the existing label by CTRL+LMB on it. "
                 << "\n";
            return;
        }
    }
    viewer->iren->Render();

    // Forward events
    vtkInteractorStyleTrackballActor1::OnRightButtonDown();
}

// Negative feedback controller
// Makes sure that holding right mouse button down doesn't
// continuously create labels
void ActorSelector::OnRightButtonUp()
{
    labelInhibited = false;

    vtkInteractorStyleTrackballActor1::OnRightButtonUp();
}

// Switches back to interactor syle when ctrl key is released
void ActorSelector::OnKeyUp()
{
    vtkRenderWindowInteractor *rwi = this->Interactor;
    string key = rwi->GetKeySym();

    if (key == "Control_L")
    {
        viewer->iren->SetInteractorStyle(viewer->interactor);
    }

    // Forward event
    vtkInteractorStyleTrackballActor1::OnKeyUp();
}

// Gets click position on window
void ActorSelector::SetPos()
{
    x = this->Interactor->GetEventPosition()[0];
    y = this->Interactor->GetEventPosition()[1];

    this->FindPokedRenderer(x, y);
    this->FindPickedActor(x, y);
}

// Gets the 3D click position on the actual volume
double *ActorSelector::PickOnVolume(vtkCellPicker *_picker)
{
    _picker->SetTolerance(0.05);

    // Pick from this location.
    _picker->Pick(x, y, 0, this->GetDefaultRenderer());

    double *_worldPosition = _picker->GetPickPosition();
    std::cout << "Cell id is: " << _picker->GetCellId() << std::endl;

    if (_picker->GetCellId() != -1)
    {
        std::cout << "Pick position is: " << _worldPosition[0] << " " << _worldPosition[1]
                  << " " << _worldPosition[2] << endl;

        vtkSmartPointer<vtkIdTypeArray> ids =
            vtkSmartPointer<vtkIdTypeArray>::New();
        ids->SetNumberOfComponents(1);
        ids->InsertNextValue(_picker->GetCellId());

        vtkSmartPointer<vtkSelectionNode> selectionNode =
            vtkSmartPointer<vtkSelectionNode>::New();
        selectionNode->SetFieldType(vtkSelectionNode::CELL);
        selectionNode->SetContentType(vtkSelectionNode::INDICES);
        selectionNode->SetSelectionList(ids);

        vtkSmartPointer<vtkSelection> selection =
            vtkSmartPointer<vtkSelection>::New();
        selection->AddNode(selectionNode);
    }

    return _worldPosition;
}