#README

##Required packages :
    VTK-8.2
    Qt 5.11
    boost/property_tree
    
##Installation :
    cd lmml_viewer
    mkdir build
    cd build
    cmake ..
    make

##Run in terminal :
    ./LmmlViewer

    Choose rendering type (y/n)
    
##Controls : (as of 19/03/01)
    Arrows :  left/right - opacity change
              up/down - move the clipping plane 
    
    [ : decrease density value
    ] : increase density value 

    t : choose active tissue for opacity changing (left/right arrows)
    i : toggle between linear and nearest value interpolation

##Current issues
    During cmake phase, the compiler might not know where the VTK build is.
    You need to specify it in the environment variables with :
        
        export VTK_DIR=/path/to/VTK-Release-build/
