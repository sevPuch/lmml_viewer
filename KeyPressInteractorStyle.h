#ifndef KEYPRESSINTERACTORSTYLE_H
#define KEYPRESSINTERACTORSTYLE_H

#include <vector>
#include <string>
#include <iostream>

#include "Viewer.h"
#include "vtkInteractorStyleTrackballCamera1.h"
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkSmartPointer.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkObjectFactory.h>
#include <vtkCommand.h>

using namespace std;

class Viewer;
class KeyPressInteractorStyle : public vtkInteractorStyleTrackballCamera1
{
public:
  Viewer *viewer;
  vtkActor *testLabel;
  vector<vtkSmartPointer<vtkActor>> labels;

  bool canLabel = false;
  bool labelInhibited = false;

  float opacityChangeRate = 0.05f;
  int densityBrowsingRate = 50;
  float maxOpacity = 0.95f;
  float minOpacity = 0.05f;
  float pushForce = 1.5f;
  int opacityPeakRadius = 500; // Advised value : 500
  int opacityCutOff = 1000;
  bool switchBool = false;
  
  bool isClicking = false;

  string jsonFileName;
  string CT_FileName;
  string currentDensity;
  vector<string> allTissues;
  string currentTissue;
  string presetName;
  double opacityToUse;

  static KeyPressInteractorStyle *New();
  vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleTrackballCamera1);

  void OnKeyPress();
  void OnKeyUp();
  // void OnLeftButtonDown();
  // void OnLeftButtonUp();
  // void OnRightButtonDown();
  // void OnRightButtonUp();
  void SetViewer(Viewer *_viewer);
  void OnLeftButtonDown();
  void OnLeftButtonUp();
  void OnMouseMove();

  KeyPressInteractorStyle() {}
};

#endif