#include "Viewer.h"
#include "KeyPressInteractorStyle.h"
#include "ActorSelector.h"

#include <string>
#include <iostream>
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkMetaImageReader.h>
#include <vtkNamedColors.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkMarchingCubes.h>
#include <vtkClipVolume.h>

#include <vtkPlaneSource.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include <vtkSphereSource.h>
#include <vtkProbeFilter.h>
#include <vtkSphere.h>
#include <vtkClipDataSet.h>
#include <vtkImplicitVolume.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#include <vtkPlane.h>
#include <vtkVersion.h>
#include <vtkRendererCollection.h>
#include <vtkIdTypeArray.h>
#include <vtkTriangleFilter.h>
#include <vtkCommand.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>

#include <array>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include "vtkInteractorStyleTrackballCamera1.h"
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>

using namespace std;

KeyPressInteractorStyle *KeyPressInteractorStyle::New()
{
    return new KeyPressInteractorStyle;
}

// Manages user input for everything except labels
void KeyPressInteractorStyle::OnKeyPress()
{
    // Get the keypress
    vtkRenderWindowInteractor *rwi = this->Interactor;
    string key = rwi->GetKeySym();

    // Output the key that was pressed
    std::cout << "Pressed " << key << std::endl;

    // Handle an arrow key
    if (key == "bracketright")
    {
        UpdateDensities(1);
    }
    if (key == "bracketleft")
    {
        UpdateDensities(-1);
    }

    if (key == "Up")
    {
        MovePlaneDown(volume);
    }
    if (key == "Down")
    {
        MovePlaneUp(volume);
    }

    if (key == "i")
    {
        SwitchInterpolationMode();
    }

    if (key == "t")
    {
        ChangeCurrentTissue();
    }

    if (key == "Control_L")
    {
        canLabel = true;
        viewer->iren->SetInteractorStyle(viewer->selector);
    }

    // Forward events
    vtkInteractorStyleTrackballCamera1::OnKeyPress();
}

void KeyPressInteractorStyle::OnKeyUp()
{
    // Forward events
    vtkInteractorStyleTrackballCamera1::OnKeyUp();
}

void KeyPressInteractorStyle::SetViewer(Viewer *_viewer)
{
    this->viewer = _viewer;
}

// Checks if clicking on the main renderer, if so, set bool to true for OnMouseMove events.
void KeyPressInteractorStyle::OnLeftButtonDown()
{
    int xi = this->Interactor->GetEventPosition()[0];
    int yi = this->Interactor->GetEventPosition()[1];

    vtkRenderer *pokedRen = this->viewer->iren->FindPokedRenderer(xi, yi);
    if (pokedRen == this->viewer->ren)
    {
        this->isClicking = true;
    }
    else
    {
        // Do stuff for infoRen interaction
    }

    vtkInteractorStyleTrackballCamera1::OnLeftButtonDown();
}

// Detect continuous clicking 
void KeyPressInteractorStyle::OnLeftButtonUp()
{
    this->isClicking = false;
    vtkInteractorStyleTrackballCamera1::OnLeftButtonUp();
}

// Updates label positions on left mouse click + move
void KeyPressInteractorStyle::OnMouseMove()
{
    if (isClicking == false)
        return;

    int xi = this->Interactor->GetEventPosition()[0];
    int yi = this->Interactor->GetEventPosition()[1];

    this->viewer->camera->SetViewUp(0, 0, -1);

    for (Label *label : this->viewer->labels)
    {
        label->UpdatePoint2();
    }

    vtkInteractorStyleTrackballCamera1::OnMouseMove();
}