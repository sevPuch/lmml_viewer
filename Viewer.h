#ifndef VIEWER_H
#define VIEWER_H

#include "Viewer.h"
#include "KeyPressInteractorStyle.h"
#include "ActorSelector.h"
#include "Label.h"

#include <string>
#include <vtkVolume.h>
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkMetaImageReader.h>
#include <vtkNamedColors.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkMarchingCubes.h>
#include <vtkClipVolume.h>

#include <vtkPlaneSource.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include <vtkSphereSource.h>
#include <vtkProbeFilter.h>
#include <vtkSphere.h>
#include <vtkClipDataSet.h>
#include <vtkImplicitVolume.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#include <vtkPlane.h>
#include <vtkVersion.h>
#include <vtkRendererCollection.h>
#include <vtkIdTypeArray.h>
#include <vtkTriangleFilter.h>
#include <vtkCommand.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>

#include <array>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include "vtkInteractorStyleTrackballCamera.h"
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>
#include <vtkAxesActor.h>

using namespace std;

extern vtkSmartPointer<vtkVolume> volume;

class ActorSelector;
class KeyPressInteractorStyle;
class Label;
class Viewer
{
public:
  static Viewer *New();
  KeyPressInteractorStyle *interactor;
  ActorSelector *selector;
  vector<Label *> labels;
  vector<vtkTextActor *> texts;

  vtkSmartPointer<vtkRenderWindowInteractor> iren;
  vtkSmartPointer<vtkRenderWindow> renWin;
  vtkSmartPointer<vtkRenderer> ren;

  // Info renderer :
  vtkSmartPointer<vtkRenderer> infoRen;
  vtkSmartPointer<vtkTextActor> title;
  vtkSmartPointer<vtkTextActor> content;
  vtkSmartPointer<vtkTextActor> footer;
  int titleSize = 20;
  int contentSize = 12;
  int footerSize = 14;
  int *winSize;
  double *leftViewport;
  double *rightViewport;

  vtkSmartPointer<vtkCamera> camera;
  vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> volumeMapper;

  double *volumeCenter;

  void TweakInteractorStyle();
  void SetInteractor(KeyPressInteractorStyle *_interactor);
  void SetSelector(ActorSelector *_selector);
  void SetUpInfoRenderer();
  void SetText(int element, string _text); // elements: 0 = title, 1 = content, 2 = footer
};

void UpdateOpacities(float _opacityToUse, string _tissue);
void UpdateDensities(int sign);

void InitializePresets(string _tag);
void UpdatePresets(string _tag);

void TuneOpacityPeaks();
void MovePlaneUp(vtkSmartPointer<vtkVolume> _volume);
void MovePlaneDown(vtkSmartPointer<vtkVolume> _volume);
void SwitchInterpolationMode();
void ChangeCurrentTissue();
vector<string> ReadJson(string _jsonFileName);
void CatchTags(vector<string> allTags);

#endif